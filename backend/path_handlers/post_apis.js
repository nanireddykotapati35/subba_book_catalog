import { response } from "express";
import { request } from "express";
import { CREATE_BOOK_DETAILS_DATA } from "../utils/model";

const add_book_hanlder = (req, res) => {
  const { name, description, author, discount,price, stock, publisher } = req.body;

  const book_details = { name, description,discount, author, price, stock, publisher };

  //creating an instance of BookModel
  const new_book = new CREATE_BOOK_DETAILS_DATA(book_details);

  // saving the book details into the database
  new_book.save((err, result) => {
    if (err) {
      res.status(500).send("Unable to add book, server error! " + err);
    } else {
      res.status(200).send("Book Added successfully");
    }
  });
};

export default add_book_hanlder;
