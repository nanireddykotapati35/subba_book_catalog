import mongoose from "mongoose";

const postBookDetails=new mongoose.Schema({
  name:{
    type: String,
    required: true,
    unique:true
  },
  description:{
    type: String,
    required: true,
  },
  price:{
    type: Number,
    required: true,
  },
  discount:Number,
  stock:{
    type: Number,
    required: true,
  },
  author:{
    type: String,
    required: true,
  },
  publisher:{
    type: String,
    required: true,
  },
});


export default postBookDetails;