import { CREATE_BOOK_DETAILS_DATA } from "../utils/model";
/**
 * This handler is used for checking uniqueness of the book
 * @param {*} requests name as body params for verifying if the book is already present in database
 * @param {*} response sends status_code: 409 if the book is there in database, otherwise
 *                     calls middleware function next() for further process
 */
const db_validator = (req, res, next) => {
  const { name} = req.body;

  CREATE_BOOK_DETAILS_DATA.findOne({name:name},(err,data)=> {
    if (err) {
        res.status(404).send("Data Not Fetched")
    }
    else {
        //checking whether the book entered is there in store or not
        let count = 0;
        for (let key in data) {
            count += 1;
        }
        //if count is zero book is not available
        if (count === 0) {
           next()
        }
        else {
            res.status(409).send("Book is already present in store")
        }
    }
})

  
};

export default db_validator;
