import './App.css';
// import "../node_modules/bootstrap/dist/css/bootstrap.min.css"
// import "../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js'
import Navbaar from './components/Navbaar';
import Home from './components/Home';
import AddBookPage from './components/register/Register'

import Edit from './components/Edit';
import Details from './components/Details';
import {Switch,Route} from "react-router-dom";
import bookHome from './components/bookHome/bookHome';




function App() {
  return (
   <>
    {/* <Navbaar /> */}
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/register" component={AddBookPage} />
      <Route exact path="/edit/:id" component={Edit} />
      <Route exact path="/view/:id" component={Details} />
      <Route exact path="/bookHome" component={bookHome}/>
      
    </Switch>
   
   </>
  );
}

export default App;






