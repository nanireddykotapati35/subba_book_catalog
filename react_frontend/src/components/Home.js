import React, { useState, useEffect, useContext } from 'react'
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import CreateIcon from '@mui/icons-material/Create';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import { NavLink } from 'react-router-dom';
import { adddata, deldata } from './context/ContextProvider';
import { updatedata } from './context/ContextProvider'
import { searchDataPro } from './context/ContextProvider';
import Navbaar from './Navbaar';
import AlertDialogSlide from './popup/popup';




const Home = () => {

    const [getuserdata, setUserdata] = useState([]);

    const [searchInput, setSearchInput] = useState("");

    const { udata, setUdata } = useContext(adddata);

    const { updata, setUPdata } = useContext(updatedata);

    const { searchData, setSearchData } = useContext(searchDataPro);

    const { dltdata, setDLTdata } = useContext(deldata);

    const onChangeSearchInput = (event) => {
        setSearchInput(event.target.value);
    }

    const getdata = async () => {
        console.log("hello")
        const res = await fetch("http://localhost:2000/getdata", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        });


        const data = await res.json();


        if (!data) {
            console.log("error ");

        } else {

            setUserdata(data)
            console.log("get data");

        }
    }

    useEffect(() => {
        getdata();
    }, [])

    const deleteuser = async (id) => {

        const res2 = await fetch(`http://localhost:2000/deleteuser/${id}`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        });

        const deletedata = await res2.json();
        console.log(deletedata);

        if (!deletedata) {
            console.log("error");
        } else {
            console.log("user deleted");
            setDLTdata(deletedata)
            getdata();
        }

    }
    const searchResults = getuserdata.filter(eachUser =>

        eachUser.name.includes(searchData)
    )

    return (

        <>
            <Navbaar />

            {
                udata ?
                    <>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{udata.name}</strong>  added succesfully!
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    </> : ""
            }
            {
                updata ?
                    <>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{updata.book_name}</strong>  updated succesfully!
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    </> : ""
            }

            {
                dltdata ?
                    <>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>{dltdata.book_name}</strong>  deleted succesfully!
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    </> : ""
            }


            <div className="mt-5 ">
                <div className="container">
                    <div className="add_btn mt-2 mb-2">
                        <NavLink to="/register" className="btn btn-primary">Add data</NavLink>
                    </div>

                    <table class="table">
                        <thead>
                            <tr className="table-dark">
                                <th scope="col">Serial No</th>
                                <th scope="col">Book name</th>
                                <th scope="col">Book price</th>
                                <th scope="col">Availability</th>
                                <th scope="col">Discount</th>
                                <th scope="col">Actions</th>

                            </tr>
                        </thead>
                        <tbody>

                            {
                                searchResults.map((element, id) => {
                                    return (
                                        <>
                                            <tr>
                                                <th scope="row">{id + 1}</th>
                                                <td>{element.name}</td>
                                                <td>{element.price}</td>
                                                <td>{element.stock}</td>
                                                <td>{element.discount}</td>
                                                <td className="d-flex pad">
                                                    <NavLink to={`view/${element._id}`}> <button className="btn btn-success" ><RemoveRedEyeIcon /></button></NavLink>
                                                    <NavLink to={`edit/${element._id}`}>  <button className="btn btn-primary"><CreateIcon /></button></NavLink>
                                                    <AlertDialogSlide deleteuser = {deleteuser} id={element._id}/>
                                                </td>
                                            </tr>
                                        </>
                                    )
                                })
                            }
                        </tbody>
                    </table>


                </div>
            </div>
        </>
    )
}

export default Home

















