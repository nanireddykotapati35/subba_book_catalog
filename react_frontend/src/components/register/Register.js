// import React, { useContext, useState } from 'react'
import React, { Component } from 'react';
import {  useHistory } from 'react-router-dom'
import { adddata } from '../context/ContextProvider';
import Headeer from '../header/headar';
import './register.css'
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import {validate_book_name, validate_author_name, validate_description, validate_price, validate_stock, validate_publisher_name} from "../validations/validation"


class AddBookPage extends Component {
    constructor(props) {
        super(props);
        this.state = {  
                name: "",
                name_err: "",
                description: "",
                description_err: "",
                author: "",
                author_err: "",
                publisher: "",
                publisher_err: "",
                discount:"",
                price: "",
                price_err: "",
                stock: "",
                stock_err: "",
                success_msg: "",
                failure_msg: ""
            };
      }


    
      onChangeName = (event) => {
        this.setState({name:event.target.value})
      }

    
      onChangeDescription = (event) => {
        this.setState({description:event.target.value})
      }
    
      onChangeAuthor = (event) => {
        this.setState({author: event.target.value})
      }
    
      onChangePublisher = (event) => {
        this.setState({publisher: event.target.value})
      }
      
      onChangeDiscount = (event) => {
        this.setState({discount: event.target.value})
      }

    
      onChangePrice = (event) => {
        this.setState({price: event.target.value})
      }
    
      onChangeStock = (event) => {
        this.setState({stock: event.target.value})
      }
    
      submitForm = async (event) => {

        this.setState({name_err: "",
                       author_err: "",
                       description_err: "",
                       price_err: "",
                       stock_err: "",
                       publisher_err:""})
        
          const name_err = validate_book_name(this.state.name)
          if (name_err !== "Success") {
            this.setState({name_err:name_err})
          }

          const author_name_err = validate_author_name(this.state.author)
          if (author_name_err !== "Success") {
            this.setState({author_err:author_name_err})
          }

          const description_err = validate_description(this.state.description)
          if (description_err !== "Success") {
            this.setState({description_err:description_err})
          }

          const price_err = validate_price(this.state.price)
          if (price_err !== "Success") {
            this.setState({price_err:price_err})
          }

          const stock_err = validate_stock(this.state.stock)
          if (stock_err !== "Success") {
            this.setState({stock_err:stock_err})
          }

          const publisher_err = validate_publisher_name(this.state.publisher)
          if (publisher_err !== "Success") {
            this.setState({publisher_err:publisher_err})
          }




        event.preventDefault()

        this.setState({ success_msg: "" });
        this.setState({ failure_msg: "" });

        const {name, author, description, publisher, price, discount, stock} = this.state
    
        const Book_Data = {name, author, description, discount,publisher, price, stock}
    
        const url = "http://localhost:2000/register"
        
        const options = {
          method: "POST",
          body: JSON.stringify(Book_Data),
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
        };  

        if (name_err === "Success" && author_name_err === "Success" && description_err === "Success"
           && price_err === "Success" && stock_err === "Success" && publisher_err === "Success") {
          try {
            const response = await fetch(url, options);
          
            if (response.status === 200) {
              const msg = "Book Added Successfully"
              this.setState({success_msg: msg})
              alert(msg)
            }
            else {
              const msg = "Book Not Added"
              this.setState({failure_msg: msg})
              alert(msg)
            }
          }
          catch {
            const msg = "unable to connect the server"
            this.setState({failure_msg: msg})
            alert(msg)
          }
        }
        
    } 
    
      clearForm = () => {
        this.setState({
          name: "",
          description: "",
          price: "",
          stock: "",
          author: "",
          publisher: "",
          discount:"",
          name_err: "",
          description_err: "",
          price_err: "",
          stock_err: "",
          author_err: "",
          publisher_err: ""
        })
      }

    render() {
        const {name, author, description, discount,publisher, price, stock, success_msg, failure_msg, name_err, author_err, description_err, price_err, stock_err, publisher_err } = this.state
        return (
            <>
            <Headeer/>
            {/* <TopBar/> */}
            {/* <div className="d-flex flex-row"> */}
            {/* <SideBar/> */}
            <div style={{ backgroundColor: "#DCF7F7", overflow:"auto"  }} className="col-lg-12 col-md-12 col-12"  >
        {/* <Card
          className="m-3 d-flex justify-content-center"
          sx={{ maxWidth: 800 }}
          style={{ backgroundColor: "#DCF7F7" }}
        > */}
          <CardContent>
            <div className="main_sec_p col-lg-12 col-md-12 col-12">
                <h1 className="welcome_msg_pg text-center d-none d-md-block"> Add Book </h1>
                <h1 className="welcome_msg_pg_medium text-center d-block d-md-none"> Add Book </h1>
                <div className='d-flex justify-content-center'>
                <table className="ts_table">
                <tbody>
                    <tr className="ts_row">
                    <th className="ts_head">Description</th>
                    <th className="ts_head">Enter Details</th>
                    </tr>
                    <tr className="ts_row">
                    <td className="ts_col col-lg-12 col-md-12 col-12">Book Name</td>
                    <td className="ts_col">
                        <input type="text" pattern="[A-Za-z]{10}" title="3 alphabets characters only" onChange={this.onChangeName}  value={name}/>
                        <p className='mb-0 validation_error'> {name_err} </p>
                    </td>
                    </tr>
                    <tr className="ts_row ">
                    <td className="ts_col col-lg-12 col-md-12 col-12">Description</td>
                    <td className="ts_col ">
                        <input type="text" onChange={this.onChangeDescription} value={description}/>
                        <p className='mb-0 validation_error'> {description_err} </p>
                    </td>
                    </tr>
                    <tr className="ts_row">
                    <td className="ts_col col-lg-12 col-md-12 col-12">Author</td>
                    <td className="ts_col ">
                        <input type="text" onChange={this.onChangeAuthor} value={author}/>
                        <p className='mb-0 validation_error'> {author_err} </p>
                    </td>
                    </tr>
                    <tr className="ts_row">
                    <td className="ts_col col-lg-12 col-md-12 col-12">Publisher</td>
                    <td className="ts_col ">
                        <input type="text" onChange={this.onChangePublisher} value={publisher}/>
                        <p className='mb-0 validation_error'> {publisher_err} </p>
                    </td>
                    </tr>
                    <tr className="ts_row">
                    <td className="ts_col col-lg-12 col-md-12 col-12">Discount</td>
                    <td className="ts_col ">
                        <input type="text" onChange={this.onChangeDiscount} value={discount}/>
                        <p className='mb-0 validation_error'> {publisher_err} </p>
                    </td>
                    </tr>
                    <tr className="ts_row">
                    <td className="ts_col col-lg-12 col-md-12 col-12">Price</td>
                    <td className="ts_col ">
                        <input type="text" onChange={this.onChangePrice} value={price}/>
                        <p className='mb-0 validation_error'> {price_err} </p>
                    </td>
                    </tr>
                    <tr className="ts_row">
                    <td className="ts_col col-lg-12 col-md-12 col-12">Stock</td>
                    <td className="ts_col ">
                        <input type="text" onChange={this.onChangeStock} value={stock}/>
                        <p className='mb-0 validation_error'> {stock_err} </p>
                    </td>
                    </tr>
                </tbody>
                </table>
                <h1 className='success_msg text-center'> {success_msg}</h1>
                <h1 className='failure_msg text-center'> {failure_msg}</h1>
                </div>
                </div>
                {/* </div> */}
                  <div className=' d-flex  justify-content-center' style={{paddingRight:"57px"}}>
                <button className="button_style_p " onClick={this.clearForm}> Clear All </button>
                <button  className="button_style_p_m " onClick={this.submitForm}> Add Book </button>
                </div>
                </CardContent>
                 </div>

                {/* <div className='d-flex  justify-content-end'>
                <div className="add_button">
                <button className='button_style_p' onClick={this.clearForm}> Clear All </button>
                <button  className='button_style_p' onClick={this.submitForm}> Add Book </button>
                </div>
                </div> */}
               
            {/* </div> */}
            {/* </div> */}
            {/* </Card> */}
            </>
        );
    }
}

export default AddBookPage;


