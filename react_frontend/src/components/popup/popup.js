import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function AlertDialogSlide(props) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleConform = () => {
    const{deleteuser,id} = props
    setOpen(false);
    deleteuser(id)
    
  };

  const handleClose = ()=>{
    setOpen(false);
  }

  return (
    <div style={{backgroundColor:"lightgreen"}}>
      <button className="btn btn-danger" onClick={handleClickOpen} ><DeleteOutlineIcon /></button>
      <Dialog
        style={{backgroundColor:"lightgreen"}}
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle>{"Are You sure Want to Delete"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            Deleted From the Book List
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleConform}>Confirm</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
