import React, { useEffect, useState } from "react";
import CreateIcon from "@mui/icons-material/Create";
import { Button } from "@mui/material";

import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import MenuBookIcon from "@mui/icons-material/MenuBook";
import DiscountIcon from "@mui/icons-material/Discount";
import CurrencyRupeeIcon from "@mui/icons-material/CurrencyRupee";
import BorderColorIcon from "@mui/icons-material/BorderColor";
import EventAvailableIcon from "@mui/icons-material/EventAvailable";
import DescriptionIcon from "@mui/icons-material/Description";
import PublishedWithChangesIcon from "@mui/icons-material/PublishedWithChanges";
// import LocationOnIcon from '@mui/icons-material/LocationOn';
import { NavLink, useParams, useHistory } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
import Rate from "./rating";
import Headeer from "./header/headar";


const Details = () => {
  const [getuserdata, setUserdata] = useState([]);
  console.log(getuserdata);

  const { id } = useParams("");
  console.log(id);

  const history = useHistory();

  const getdata = async () => {
    const res = await fetch(`http://localhost:2000/getuser/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data = await res.json();
    console.log(data);

    if (res.status === 422 || !data) {
      console.log("error ");
    } else {
      setUserdata(data);
      console.log("get data");
    }
  };

  useEffect(() => {
    getdata();
  }, []);

  const deleteuser = async (id) => {
    const res2 = await fetch(`http://localhost:2000/deleteuser/${id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    });

    const deletedata = await res2.json();
    console.log(deletedata);

    if (res2.status === 422 || !deletedata) {
      console.log("error");
    } else {
      console.log("user deleted");
      history.push("/");
    }
  };

  return (
    <>
    <Headeer/>
      <div style={{ backgroundColor: "#DCF7F7" }} className="d-flex flex-row">
        <Card
          className="m-3"
          sx={{ maxWidth: 800 }}
          style={{ backgroundColor: "#DCF7F7" }}
        >
          <CardContent>
              {/* <Button href="/">
          <svg
          xmlns="http://www.w3.org/2000/svg" width="25" height="20" fill="black" class="bi bi-back" viewBox="0 0 16 16">
  <path d="M0 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v2h2a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2v-2H2a2 2 0 0 1-2-2V2zm2-1a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H2z" />
</svg>
</Button> */}
         <div className="d-flex flex-row flex-end">
           <h1
                style={{
                  fontWeight: 400,
                  fontFamily: "Roboto",
                  fontSize: "40px",
                }}
              >
                {getuserdata.name}
              </h1>
              <NavLink to={`/edit/${getuserdata._id}`}>
                {""}
                <button className="btn btn-primary mx-2"
                 style={{width:"41px",height:"39px"}}>
                  <CreateIcon />
                </button>
              </NavLink>
              <button
                className="btn btn-danger"
                style={{width:"41px",height:"39px"}}
                onClick={() => deleteuser(getuserdata._id)}
              >
                <DeleteOutlineIcon />
              </button>
              </div>
              <div className="add_btn">

              <img
                    src="https://gifimage.net/wp-content/uploads/2017/10/book-flipping-pages-gif-10.gif"
                    style={{ width: 300, height: 455 }}
                    alt="profile"
                  />
            </div>
          </CardContent>
        </Card>
        <Card
          className="m-3"
          sx={{ width:"40%" }}
          style={{ backgroundColor: "#DCF7F7" }}
        >
          <CardContent>
            <div>
            <div className="row d-flex justify-content-left">
              
              {/* <div className="left_view  col-lg-6 col-md-6 col-12"> */}
                  
                  {/* <div className="right_view  col-lg-6 col-md-6 col-12"> */}
  
                  <h3 className="mt-5">
                    <MenuBookIcon
                      style={{ marginRight: "5px", color: "brown" }}
                    />{" "}
                    Book name: {getuserdata.name}
                  </h3>
                  <h3 className="mt-3">
                    <CurrencyRupeeIcon
                      style={{ marginRight: "7px", color: "brown" }}
                    />
                    Book price: <span>{getuserdata.price}</span>
                  </h3>
                  <h3 className="mt-3">
                    <DiscountIcon
                      style={{ marginRight: "5px", color: "brown" }}
                    />
                    <span />
                    Discount: <span>{getuserdata.discount}</span>
                  </h3>
                  <h3 className="mt-3">
                    <EventAvailableIcon
                      style={{ marginRight: "7px", color: "brown" }}
                    />
                    Availability: <span> {getuserdata.stock}</span>
                  </h3>
                 
                  <h3 className="mt-3">
                    <BorderColorIcon
                      style={{ marginRight: "7px", color: "brown" }}
                    />
                    Auther name: <span>{getuserdata.author}</span>
                  </h3>
                
                 
                  <h3 className="mt-3">
                    <PublishedWithChangesIcon
                      style={{ marginRight: "7px", color: "brown" }}
                    />
                    Publisher name: <span>{getuserdata.publisher}</span>
                  </h3>
                  <h3 className="mt-3">
                    <DescriptionIcon
                      style={{ marginRight: "5px", color: "brown" }}
                    />
                    Description: <span>{getuserdata.description}</span>
                  </h3>

                </div>
            </div>
          </CardContent>
        </Card>
      </div>
    </>
  );
};

export default Details;
